Amanda Minieri
Red Id:  807299266
CS583 Spring 2015
4-15-15
Title:  Fruit And Veggies 
 Type of Game:   2D Match3 Video Game
BitBucket:  homeaminieri/2D_Match3_Game

Screen Resolution Requirement: 720x480
(only this screen resolution was provided for
so when BUILD and RUN, be sure to select this
resolution)

Two Game Play Levels
Second level more difficult by time constraint.

First Scene (play button scene):
1. PLAY button starts game play.
2. QUIT button quits game.  (NOTE: QUIT button does nothing in unity editor.
   only with BUILD and RUN will the QUIT button execute)
3. SAVE/LOAD button goes to the save/load scene.
4. About button goes to about scene.  ESC returns to main menu.

Save/Load Scene:
1. The player name is required to save game for player.
2. The SAVE button saves the current score and elapsed time to an XML file.
3. The LOAD button loads the last saved score and elapsed time.

Game Rules for first level:
1. A match of 3 or more adjacent icons is a match.
2. 10 points per icon matched.
3. 120 seconds to play first level (level 1).
4. 1000 points earned within 120 seconds of start of first level wins level.
5. When first level won, choice to go to second level or main menu again.
6. Press ESC at any time during the game to exit to options menu.

Game Rules for second level:
1. A match of 3 or more adjacent icons is a match.
2. 10 points per icon matched.
3. 90 seconds to play second level (level 2).
4. 1000 points earned within 90 seconds of start of second level wins level.
5. When second level won, choice to quit and go to main menu.
6. Press ESC at any time during the game to exit to options menu.

Options Menu from game levels 1 and 2 (by pressing the ESC key):
1. The options menu from the game play levels can be 
   selected by pressing ESC.
2. The RESUME button resumes game level from where it was left off.
3. The RESTART button restarts the game level from start.
4. The OPTIONS button contains a menu for the screen resolution and volume.
5. The MAIN MENU button goes back to the first scene with the play button.

Sources:
1) http://unity3d.com/learn/tutorials/modules/beginner/ui 
2) http://unity3d.com/learn/tutorials/modules/intermediate/layers 
4) http://wiki.unity3d.com/index.php/SplashScreen 
5) http://opengameart.org/content/fruit-and-vegetables 
6) http://docs.unity3d.com/manual 
7) http://answers.unity3d.com/questions 
8) http://soundbible.com/819-Checkout-Scanner-Beep.html 
9) http://opengameart.org/content/grain 
10) http://opengameart.org/content/country-field 
11) http://soundbible.com/Power-Up.html 
12) http://soundbible.com/FallandSplat.html
13) http://www.eggheadcafe.com/articles/system.xml.xmlserialization.asp 

Credit given on following sources:
1) The sprite sheet I used for the game was provided by:
   http://opengameart.org/content/fruit-and-vegetables      
2) The unity3D tutorials, manual, and questions assisted with most
   of the questions I had. I would say 90% of the questions were
   covered by the unity3D.com sites.
3) The one shot sounds were provided by:
   http://soundbible.com
   
Dedication:
This game is dedicated to my family and graduate associates.

Game Control flow:
1.Generate board
2.Select two blocks
3.Check if they are near
4.Swap position and data
5.Check for match3
6.Destroy and spawn blocks



